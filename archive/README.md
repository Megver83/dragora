# archive/ - Specific files included by the packages.

A subdirectory here is represented with the name of the program,
it can contains configuration files, headers, or substantial files
(trivial differences).  Each of them is located in their corresponding
directories to reflect from where they come from.

