#! /bin/sh -
# This script is triggered by the "power" configuration file.  This is
# described in the acpid man page.

/sbin/shutdown -h now "Power button pressed"

