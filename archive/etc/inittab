# /etc/inittab:
#
# The inittab(5) file describes which processes are started at
# boot-up and during normal operation distinguishes multiple
# runlevels, each of which can have its own set of processes
# that are started.
#
# Copyright (c) 2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Init runlevels in Dragora
#  0  =  Halt or power-off
#  1  =  Single-user mode
#  2  =  -> 3
#  3  =  Multi-user mode
#  4  =  X Window sessions
#  5  =  ^ 4
#  6  =  Reboot

# Set default runlevel
id:3:initdefault:

# System initialization
si::sysinit:/etc/rc.d/rc.startup

# Runlevel scripts
r0:0:wait:/etc/rc.d/rc.shutdown
r1:1S:wait:/etc/rc.d/rc.single
r3:2345:wait:/etc/rc.d/rc.multiuser
r4:45:respawn:/etc/rc.d/rc.x11
r6:6:wait:/etc/rc.d/rc.reboot

# What to do when the "magic keys" are pressed
ca::ctrlaltdel:/sbin/shutdown -t 3 -r now

# Virtual consoles to log in
c1:123:respawn:/sbin/agetty --noclear	tty1 38400 linux
c2:123:respawn:/sbin/agetty		tty2 38400 linux
c3:12345:respawn:/sbin/agetty		tty3 38400 linux
#c4:12345:respawn:/sbin/agetty		tty4 38400 linux
#c5:12345:respawn:/sbin/agetty		tty5 38400 linux
#c6:12345:respawn:/sbin/agetty		tty6 38400 linux

# Local serial lines
#s1:12345:respawn:/sbin/agetty -L always	ttyS0 115200 vt100
#s2:12345:respawn:/sbin/agetty -L always	ttyS1 115200 vt100
