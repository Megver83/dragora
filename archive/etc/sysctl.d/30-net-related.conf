# /etc/sysctl.d/30-net-related.conf - System control for the network.
#
# Values: [0] DISABLE, [1] ENABLE.
#

# Forward Packets between interfaces.
#
# This variable is special, its change resets all configuration
# parameters to their default state (RFC1122 for hosts, RFC1812
# for routers):
#
net.ipv4.ip_forward = 0

# Reverse path filter.
#
# 0 - No source validation.
# 1 - Strict mode as defined in RFC3704 Strict Reverse Path
#     Each incoming packet is tested against the FIB and if the interface
#     is not the best reverse path the packet check will fail.
#     By default failed packets are discarded.
# 2 - Loose mode as defined in RFC3704 Loose Reverse Path
#     Each incoming packet's source address is also tested against the FIB
#     and if the source address is not reachable via any interface
#     the packet check will fail.
#
net.ipv4.conf.default.rp_filter = 0
net.ipv4.conf.all.rp_filter = 0

# ICMP ECHO requests.
#
# If set true, then the kernel will ignore all ICMP ECHO,
# this to prevent a "smurf" attack:
#
net.ipv4.icmp_echo_ignore_broadcasts = 1

# TCP SYN cookie.
#
# Only valid when the kernel was compiled with CONFIG_SYNCOOKIES
# Send out syncookies when the syn backlog queue of a socket
# overflows. This is to prevent against the common
# 'SYN flood attack':
#
net.ipv4.tcp_syncookies = 1

# TCP timestamps.
#
# Enable timestamps as defined in RFC1323:
#
net.ipv4.tcp_timestamps = 1

# Log martians.
#
# Log packets with impossible addresses to kernel log:
#
net.ipv4.conf.all.log_martians = 1
