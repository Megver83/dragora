# patches/ - Patches files.

This directory contains specific differences for programs used
in the build process.  Each subdirectory is represented with
the name of the program.

