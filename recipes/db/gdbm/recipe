# Build recipe for gdbm.
#
# Copyright (c) 2016-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=gdbm
version=1.18.1
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/db"

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=ftp://ftp.gnu.org/gnu/gdbm/$tarname

description="
The GNU database library.

GNU dbm is a set of database routines that use extendible hashing
and works similar to the standard UNIX dbm routines.
"

homepage=http://www.gnu.org/software/gdbm
license=GPLv3+

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS NOTE-WARNING README THANKS"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --enable-static=yes \
     --enable-shared=yes \
     --with-readline=yes \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1 && make -j${jobs} check
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

