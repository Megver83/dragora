# Build recipe for automake.
#
# Copyright (c) 2016-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=automake
version=1.16.1
arch=noarch
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/devel"

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=ftp://ftp.gnu.org/gnu/automake/$tarname

description="
A tool to generate Makefiles automatically.

Automake is a tool for automatically generating 'Makefile.in'
files compliant with the GNU Coding Standards.

Automake requires Perl and Autoconf.
"

homepage=http://www.gnu.org/software/automake
license=GPLv2+

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS README THANKS"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Replace hard links with soft links
    (
        cd "${destdir}/usr/bin" || exit 1
        ln -sf aclocal-"${version%.*}" aclocal
        ln -sf automake-"${version%.*}" automake
    )

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"

    for file in $docs
    do
        cp -p $file "${destdir}${docsdir}"
    done
}

