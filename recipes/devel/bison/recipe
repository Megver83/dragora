# Build recipe for bison.
#
# Copyright (c) 2015-2019 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=bison
version=3.3.2
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/devel"

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=http://ftp.gnu.org/gnu/bison/$tarname

description="
GNU parser generator.

Bison is a general-purpose parser generator that converts an annotated
context-free grammar into a deterministic LR or generalized LR (GLR)
parser employing LALR(1) parser tables.  As an experimental feature,
bison can also generate IELR(1) or canonical LR(1) parser tables.
Once you are proficient with Bison, you can use it to develop a wide
range of language parsers, from those used in simple desk calculators
to complex programming languages.
"

homepage=https://www.gnu.org/software/bison
license=GPLv3+

# Source documentation
#
# The documentation files are installed by Bison.
#
#docs="AUTHORS COPYING ChangeLog NEWS README* THANKS TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS -static" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --docdir=$docsdir \
     --enable-threads=posix \
     --enable-nls \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} install DESTDIR="$destdir"

    # Delete generated charset.alias
    rm -f "${destdir}/usr/lib${libSuffix}/charset.alias"

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress man pages
    if test -d "${destdir}/$mandir"
    then
        lzip -9 "${destdir}/${mandir}"/man?/*
    fi
}

