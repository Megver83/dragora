# Build recipe for m4.
#
# Copyright (c) 2015-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=m4
version=1.4.18
release=2

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/devel"

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=http://ftp.gnu.org/gnu/m4/$tarname

description="
Traditional Unix macro processor.

GNU M4 is an implementation of the traditional Unix macro processor.
It is mostly SVR4 compatible although it has some extensions (for
example, handling more than 9 positional parameters to macros).

GNU M4 also has built-in functions for including files, running shell
commands, doing arithmetic, etc.

GNU M4 is a macro processor in the sense that it copies its input
to the output expanding macros as it goes.
"

homepage=http://www.gnu.org/software/m4
license=GPLv3+

# Source documentation
docs="AUTHORS BACKLOG COPYING ChangeLog NEWS README THANKS TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    patch -Np1 -i "${worktree}/patches/m4/update-freadahead"

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS -static" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} install DESTDIR="$destdir"

    # This look wrong since m4 does not support any encoding translation
    rm -f "${destdir}/usr/lib${libSuffix}/charset.alias"
    rmdir "${destdir}/usr/lib${libSuffix}/" 2> /dev/null || true

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress man pages
    lzip -9 "${destdir}/${mandir}/man1/m4.1"

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    for file in $docs
    do
        cp -p $file "${destdir}${docsdir}"
    done
}

