# Build recipe for python2.
#
# Copyright (c) 2017 Mateus P. Rodrigues <mprodrigues@dragora.org>.
# Copyright (c) 2017-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=Python
version=2.7.15
short_version=2.7
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/devel"

tarname=${program}-${version}.tgz

# Remote source(s)
fetch=http://www.python.org/ftp/python/${version}/$tarname

pkgname=python2

description="
Multi-paradigm programming language (v2).

Python is an interpreted, interactive object-oriented programming
language suitable (amongst other uses) for distributed application
development, scripting, numeric computing and system testing.  Python
is often compared to Tcl, Perl, Java, JavaScript, Visual Basic or
Scheme.
"

homepage=http://www.python.org/
license="Python Software Foundation License"

# Source documentation
docs="LICENSE README"
docsdir="${docdir}/${pkgname}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Use system libraries instead of the bundle ones
    rm -rf Modules/expat Modules/zlib Modules/_ctypes/libffi*

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" OPT="" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --docdir=$docsdir \
     --build="$(cc -dumpmachine)" \
     --enable-shared \
     --enable-ipv6 \
     --enable-unicode=ucs4 \
     --with-ensurepip=yes \
     --with-threads \
     --with-valgrind \
     --with-system-expat \
     --with-system-ffi

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Include the Python tools under site-packages
    TOOL_DIR=/usr/lib/python${short_version}/site-packages
    (
        cd Tools || exit 2

        # Do not clobber README file in site-packages directory
        test -f README && mv -f README README.Tools

        cp -rP ./* "${destdir}${TOOL_DIR}/"
    )

    # Make some useful symlinks at usr/bin
    (
        cd "${destdir}/usr/bin" || exit 2

        ln -sf "${TOOL_DIR}/i18n/msgfmt.py" msgfmt.py
        ln -sf "${TOOL_DIR}/i18n/pygettext.py" pygettext.py
        ln -sf "${TOOL_DIR}/pynche/pynche" pynche
    )
    unset TOOL_DIR

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}/"
}

