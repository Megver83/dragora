# Build recipe for libcap.
#
# Copyright (c) 2016-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=libcap
version=2.26
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/libs"

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=http://mirrors.edge.kernel.org/pub/linux/libs/security/linux-privs/libcap2/$tarname

description="
Library with capabilities for POSIX.1e.

The libcap package implements the user-space interfaces to the
POSIX 1003.1e capabilities available in Linux kernels.

These capabilities are a partitioning of the all powerful root
privilege into a set of distinct privileges.
"

homepage=http://sites.google.com/site/fullycapable
license=GPLv2+

# Source documentation
docs="CHANGELOG License README"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    make DEBUG="" CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS"

    make install RAISE_SETFCAP=no \
                 DESTDIR="$destdir" \
                 LIBDIR=/usr/lib${libSuffix} \
                 SBINDIR=/usr/sbin \
                 PKGCONFIGDIR=/usr/lib${libSuffix}/pkgconfig

    # Fix permissions of execution
    chmod 755 "${destdir}/usr/lib${libSuffix}/libcap.so"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

