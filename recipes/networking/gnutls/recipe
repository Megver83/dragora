# Build recipe for gnutls.
#
# Copyright (c) 2018 Markus Tornow, <tornow@riseup.net>.
# Copyright (c) 2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=gnutls
version=3.6.5
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/networking"

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=http://www.gnupg.org/ftp/gcrypt/gnutls/v3.6/"$tarname"

description="
The GnuTLS Transport Layer Security Library.

GnuTLS is a secure communications library implementing the SSL, TLS and
DTLS protocols and technologies around them.  It provides a simple C
language application programming  interface (API) to access the secure
communications protocols as well as APIs to parse and write X.509,
PKCS #12, and other required structures.

The project strives to provide a secure communications back-end, simple
to use and integrated with the rest of the base Linux libraries.  A
back-end designed to work and be secure out of the box, keeping the TLS
and PKI complexity outside the applications. 
"

homepage=http://gnutls.org/
license="LGPLv2+, GPLv3+, GFDL1.3"

# Source documentation
docs="AUTHORS CONTRIBUTING.md ChangeLog LICENSE NEWS README.md THANKS"
docsdir="${docdir}/${program}-${version}"
 
build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # TODO: `--enable-guile' does not compile under guile-2.2.4.
    # Disable it for now.

    ./configure \
    CFLAGS="$QICFLAGS" CXXFLAGS="$QICXXFLAGS" LDFLAGS="$QILDFLAGS" \
    $configure_args \
    --libdir=/usr/lib${libSuffix} \
    --infodir=$infodir \
    --mandir=$mandir \
    --docdir=$docsdir \
    --with-html-dir=${docsdir}/html \
    --enable-gtk-doc-html=no \
    --enable-static=no \
    --disable-guile \
    --with-p11-kit \
    --without-included-libtasn1 \
    --without-included-unistring \
    --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}/"
}

