# Build recipe for libnl.
#
# Copyright (c) 2017-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=libnl
version=3.4.0
release=1

pkgname=libnl3

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/networking"

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=http://github.com/thom311/libnl/releases/download/libnl3_4_0/$tarname

description="
Netlink protocol library suite.

The libnl suite is a collection of libraries providing APIs to netlink
protocol based Linux kernel interfaces.

Netlink is a IPC mechanism primarly between the kernel and user space
processes.  It was designed to be a more flexible successor to ioctl
to provide mainly networking related kernel configuration and
monitoring interfaces.
"

homepage=http://www.infradead.org/~tgr/libnl/
license=LGPLv2.1

# Source documentation
docs="COPYING ChangeLog"
docsdir="${docdir}/${pkgname}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # A patch taken from "Alpine Linux"
    patch -p1 < "${worktree}/patches/libnl3/libnl3-musl.patch"

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --docdir=$docsdir \
     --infodir=$infodir \
     --mandir=$mandir \
     --enable-shared \
     --enable-static \
     --disable-debug \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Manage dot new config file(s)
    touch "${destdir}/etc/libnl/.graft-config"

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"

    for file in $docs
    do
        cp -p $file "${destdir}${docsdir}"
    done
}

