# Build recipe for p11-kit.
#
# Copyright (c) 2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=p11-kit
version=0.23.14
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/networking"

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=http://github.com/p11-glue/p11-kit/releases/download/${version}/$tarname

description="
Provides a way to load and enumerate PKCS#11 modules.

Provides a way to load and enumerate PKCS#11 modules.  Provides a
standard configuration setup for installing PKCS#11 modules in such a
way that they're discoverable.

Also solves problems with coordinating the use of PKCS#11 by different
components or libraries living in the same process.
"

homepage=http://p11-glue.github.io/p11-glue/p11-kit.html
license="BSD 3-clause"

# Source documentation
docs="AUTHORS COPYING ChangeLog HACKING NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --enable-static=no \
     --enable-shared=yes \
     --enable-debug=no \
     --disable-rpath \
     --with-trust-paths=/etc/pki/anchors \
     --without-systemd \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

