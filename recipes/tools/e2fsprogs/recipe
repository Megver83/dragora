# Build recipe for e2fsprogs.
#
# Copyright (c) 2016-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=e2fsprogs
version=1.44.5
release=3

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/tools"

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=http://downloads.sourceforge.net/e2fsprogs/$tarname

description="
Ext2 file system utilities.

The e2fsprogs package contains the utilities for handling the ext2 file
system.  It also supports the ext3 and ext4 journaling file systems.
"

homepage=http://e2fsprogs.sourceforge.net
license="GPLv2+, LGPLv2, MIT-style licenses"

# Source documentation
docs="NOTICE README RELEASE-NOTES SUBMITTING-PATCHES"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Apply missing patch to upstream (Thanks to "Alpine Linux")
    # This patch seems a complement for:
    # http://git.kernel.org/pub/scm/fs/ext2/e2fsprogs.git/commit/?id=98c6113b414782eb5bfcb67c33d09950ed203f20
    patch -Np1 -i "${worktree}/patches/e2fsprogs/gnuc-prereq.patch"

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --enable-symlink-install \
     --enable-threads=posix \
     --enable-elf-shlibs \
     --disable-libblkid \
     --disable-libuuid \
     --disable-fsck \
     --disable-uuidd \
     --with-root-prefix=/usr \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1

    make -j${jobs} MKDIR_P="mkdir -p" DESTDIR="$destdir" install
    make -j${jobs} MKDIR_P="mkdir -p" DESTDIR="$destdir" install-libs

    # Remove generated charset.alias and generated locale.alias
    rm -f "${destdir}/usr/lib${libSuffix}/charset.alias" \
          "${destdir}/usr/share/locale/locale.alias"

    # To handle config file(s)
    touch "${destdir}/etc/.graft-config"

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/* || true
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}/"
}

