# Build recipe for gawk.
#
# Copyright (c) 2016-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=gawk
version=4.2.1
release=2

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/tools"

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch=http://ftp.gnu.org/gnu/gawk/$tarname

description="
Pattern scanning and processing language.

The awk utility interprets a special-purpose programming language
that makes it possible to handle simple data-reformatting jobs
with just a few lines of code.  It is a free version of 'awk'.

GNU awk implements the AWK utility which is part of
IEEE Std 1003.1 Shell and Utilities (XCU).
"

homepage=http://www.gnu.org/software/gawk
license=GPLv3+

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --without-libsigsegv-prefix \
     --enable-nls \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Not needed for us when 'gawk.sh' is installed
    rm -f "${destdir}/etc/profile.d/gawk.csh"

    # To handle config file via graft(1)
    touch "${destdir}/etc/profile.d/.graft-config"

    # Replace hard link with a soft link
    (
        cd "${destdir}/usr/bin" || exit 1
        ln -sf gawk-${version} gawk
    )

    # Provide manual page compatibility
    (
        cd "${destdir}/${mandir}/man1" || exit 1
        ln -sf gawk.1 awk.1
    )

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

