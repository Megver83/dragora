# Build recipe for moe.
#
# Copyright (c) 2016-2017 Matias Fonzo, <selk@dragora.org>.
# Copyright (c) 2019 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=moe
version=1.10
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/tools"

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch=http://download.savannah.gnu.org/releases/moe/$tarname

homepage=http://www.gnu.org/software/moe
license=GPLv2+

description="
The GNU moe editor.

GNU moe is a powerful, 8-bit clean, console text editor for ISO-8859
and ASCII character encodings.  It has a modeless, user-friendly
interface, online help, multiple windows, unlimited undo/redo
capability, unlimited line length, global search/replace (on all
buffers at once), block operations, automatic indentation, word
wrapping, file name completion, directory browser, duplicate removal
from prompt histories, delimiter matching, text conversion from/to
UTF-8, romanization, etc.

For more information, visit: $homepage
"

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CXXFLAGS="-Wall -W $QICXXFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --infodir=$infodir \
     --mandir=$mandir \
     --build="$(cc -dumpmachine)"

    make -j${jobs}
    make -j${jobs} DESTDIR="$destdir" install

    # To manage dot new config file
    touch "${destdir}/etc/.graft-config"

    # Compress documentation deleting redundancy

    lzip -9 "${destdir}/${infodir}/moe.info" \
            "${destdir}/${mandir}/man1/moe.1"

    rm -f "${destdir}/${infodir}/dir"

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"

    for file in $docs
    do
        cp -p $file "${destdir}${docsdir}"
    done
}

