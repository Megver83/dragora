# Build recipe for nano.
#
# Copyright (c) 2016-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=nano
version=3.2
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/tools"

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=http://www.nano-editor.org/dist/latest/$tarname

description="
The GNU nano editor.

GNU nano is designed to be a free replacement for the Pico text editor,
part of the Pine email suite from The University of Washington. It aims
to \"emulate Pico as closely as possible and perhaps include extra
functionality\".
"

homepage=http://www.nano-editor.org
license="GPLv3+, GFDL1.2"

# Source documentation
docs="AUTHORS COPYING COPYING.DOC ChangeLog NEWS README* THANKS TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS -static" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --enable-utf8 \
     --enable-nls \
     --disable-wrapping-as-root \
     --disable-glibtest \
     --with-wordbounds \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}/"
}

