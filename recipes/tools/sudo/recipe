# Build recipe for sudo.
#
# Copyright (c) 2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=sudo
version=1.8.26
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/tools"

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=http://www.sudo.ws/dist/$tarname

description="
A tool to execute a command as another user.

Sudo (su \"do\") allows a system administrator to delegate authority to
give certain users (or groups of users) the ability to run some (or
all) commands as root or another user while providing an audit trail of
the commands and their arguments.
"

homepage=http://www.sudo.ws/sudo.html
license=ISC

# Source documentation
docs="ChangeLog NEWS README doc/CONTRIBUTORS doc/HISTORY doc/LICENSE"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --localstatedir=/var \
     --enable-gcrypt \
     --disable-root-sudo \
     --with-logfac=auth \
     --with-ignore-dot \
     --with-secure-path \
     --with-env-editor \
     --with-man \
     --without-pam \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    chmod 4711 "${destdir}/usr/bin/sudo"

    # To handle config file(s) via graft(8)
    touch "${destdir}/etc/.graft-config"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

