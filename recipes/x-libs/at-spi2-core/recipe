# Build recipe for at-spi2-core.
#
# Copyright (C) 2018, MMPG <mmpg@vp.pl>
# Copyright (C) 2018, 2019 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=at-spi2-core
version=2.30.0
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/x-libs"

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=http://ftp.gnome.org/pub/gnome/sources/at-spi2-core/2.30/$tarname

description="
A part of the GNOME Accessibility Project.

It provides a Service Provider Interface for the Assistive Technologies
available on the GNOME platform and a library against which
applications can be linked.
"

homepage=http://wiki.gnome.org/Accessibility/
license=LGPLv2.1

docs="AUTHORS COPYING MAINTAINERS NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    mkdir -p build
    cd build

    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
    meson setup $configure_args \
     --libdir /usr/lib${libSuffix} \
     --buildtype=release \
     --strip \
     ..

    ninja
    DESTDIR="$destdir" ninja install

    cd ..

    # This is only for posers..  Are you a POSER(tm)?
    rm -rf "${destdir}/usr/lib/systemd"
    rmdir "${destdir}/usr/lib" || true

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

