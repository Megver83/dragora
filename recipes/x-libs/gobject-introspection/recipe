# Build recipe for GObject Introspection.
#
# Copyright (C) 2018, MMPG <mmpg@vp.pl>
# Copyright (C) 2018, 2019 Matias Fonzo <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=gobject-introspection
version=1.58.3
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/x-libs"

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=http://ftp.gnome.org/pub/gnome/sources/gobject-introspection/1.58/$tarname

description="
A layer between C libraries (using GObject) and language bindings.

GObject Introspection is a project for providing machine readable
introspection data of the API of C libraries.  This introspection data
can be used for automatic code generation for bindings, API
verification, and documentation generation.
"

homepage=http://wiki.gnome.org/Projects/GObjectIntrospection/
license="GPLv2+ | LGPLv2+, some MIT code in giscanner/"

# Copy documentation
docs="COPYING* NEWS TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --enable-static=no \
     --enable-shared=yes \
     --without-cairo \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

