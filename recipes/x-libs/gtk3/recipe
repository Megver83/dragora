# Build recipe for GTK+3.
#
# Copyright (C) 2018, MMPG <mmpg@vp.pl>
# Copyright (C) 2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=gtk+
version=3.24.1
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/x-libs"

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=http://ftp.gnome.org/pub/gnome/sources/gtk+/3.24/$tarname

description="
The version 3 of the GTK+ toolkit.

The GTK+3 package contains libraries used for creating graphical user
interfaces for applications.
"

homepage=http://www.gtk.org
license=GPLv2+

# Copy documentation
docs="AUTHORS COPYING NEWS README* HACKING"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --enable-static=no \
     --enable-shared=yes \
     --enable-introspection=yes \
     --enable-xkb=yes \
     --enable-xinerama=yes \
     --enable-xrandr=yes \
     --enable-xfixes=yes \
     --enable-xcomposite=yes \
     --enable-xdamage=yes \
     --enable-explicit-deps=yes \
     --enable-broadway-backend \
     --enable-x11-backend \
     --disable-schemas-compile \
     --disable-glibtest \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1 \
     RUN_QUERY_IMMODULES_TEST=false RUN_QUERY_LOADER_TEST=false

    make -j${jobs} \
     RUN_QUERY_IMMODULES_TEST=false RUN_QUERY_LOADER_TEST=false \
     DESTDIR="$destdir" install-strip

    # Add a basic configuration
cat << EOF > "${destdir}/etc/gtk-3.0/gtkrc"
gtk-theme-name = "Adwaita"

EOF

    # Manage dot new file(s) via graft(8)
    touch "${destdir}/etc/gtk-3.0/.graft-config"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

