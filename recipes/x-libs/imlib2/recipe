# Build recipe for imlib2.
#
# Copyright (c) 2018 Markus Tornow, <tornow@riseup.net>.
# Copyright (c) 2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=imlib2
version=1.5.1
release=2

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/x-libs"

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=http://downloads.sourceforge.net/project/enlightenment/imlib2-src/${version}/$tarname

description="
Imlib2 is the (intended) successor to Imlib.
 
Imlib2 can load image files from disk in one of many formats, save
images to disk in one of many formats, render image data onto other
images, render images to an X-Windows drawable, produce pixmaps and
pixmap masks of images, apply filters to images, rotate images,
accept RGBA data for images, scale images, and more.
"

homepage=http://www.enlightenment.org/
license=BSD

# Source documentation
docs="AUTHORS COPYING COPYING-PLAIN ChangeLog README TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --enable-mmx=no \
     --enable-amd64=no \
     --with-id3 \
     --with-x \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}/"
}

