# Build recipe for libxfce4ui.
#
# Copyright (C) 2017-2018, MMPG <mmpg@vp.pl>
# Copyright (c) 2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=libxfce4ui
version=4.12.1
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/xfce"

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=http://archive.xfce.org/src/xfce/libxfce4ui/4.12/$tarname

description="
The replacement of the old libxfcegui4 library.

It is used to share commonly used Xfce widgets among the Xfce
applications.
"

homepage=http://xfce.org
license=GPLv2v+

# Source documentation
docs="AUTHORS COPYING NEWS README THANKS TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"
    
    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .
    
    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
    $configure_args \
    --libdir=/usr/lib${libSuffix} \
    --disable-linker-opts \
    --enable-static=no \
    --enable-shared=yes \
    --enable-debug=no \
    --enable-introspection=yes \
    --with-vendor-info=Dragora \
    --with-x \
    --build="$(cc -dumpmachine)"
    
    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip
    
    # Add local vendor info

    mkdir -p "${destdir}"/usr/share/xfce4
    cat "${worktree}/archive/libxfce4ui/vendorinfo" > \
     "${destdir}"/usr/share/xfce4/vendorinfo
    
    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"/
}

