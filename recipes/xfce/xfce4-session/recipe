# Build recipe for xfce4-session.
#
# Copyright (C) 2018, MMPG <mmpg@vp.pl>
# Copyright (C) 2018-2019 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=xfce4-session
version=4.12.1
release=4

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/xfce"

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=http://archive.xfce.org/src/xfce/xfce4-session/4.12/$tarname

description="
Session manager for Xfce.

Its task is to save the state of your desktop and restore it during a
next startup.  You can create several different sessions and choose one
of them on startup.
"

homepage=http://xfce.org
license=GPLv2+

# Source documentation
docs="AUTHORS BUGS COPYING ChangeLog NEWS README TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Fix invalid option in the xrdb(1) invocation
    patch -p1 < "${worktree}/patches/xfce4-session/xinitrc-xrdb_options.patch"

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --enable-static=no \
     --enable-shared=yes \
     --enable-debug=no \
     --disable-linker-opts \
     --disable-legacy-sm \
     --disable-polkit \
     --disable-upower \
     --with-x \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Include xinitrc file to start Xfce

    mkdir -p "${destdir}/etc/X11/xinit"
    cp -p "${worktree}/archive/xfce4-session/xinitrc-xfce" \
          "${destdir}/etc/X11/xinit/"
    chmod 644 "${destdir}/etc/X11/xinit/xinitrc-xfce"

    touch "${destdir}/etc/X11/xinit/.graft-config"

    lzip -9 "${destdir}/${mandir}"/man?/*.?

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

