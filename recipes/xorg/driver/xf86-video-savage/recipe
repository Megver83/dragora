# Build recipe for xf86-video-savage.
#
# Copyright (c) 2017 Mateus P. Rodrigues <mprodrigues@dragora.org>.
# Copyright (c) 2017-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=xf86-video-savage
version=2.3.9
release=2

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/xorg/driver"

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=http://www.x.org/releases/individual/driver/$tarname

description="
S3 Savage video driver for the Xorg X server.

This package is part of X11.
"

homepage=http://www.x.org
license="MIT X Consortium"

# Source documentation
docs="COPYING ChangeLog README"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Apply patch from NetBSD in order to compile against xorg-server 1.20
    # Thanks to Thomas Klausner (http://lists.x.org/archives/xorg-devel/2018-May/056952.html)
    patch -p0 < "${worktree}/patches/xf86-video-savage/patch-src_savage__driver.c"

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     || true

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"

    for file in $docs
    do
        if test -e $file
        then
            cp -p $file "${destdir}${docsdir}"
        fi
    done
}

