# Build recipe for libinput.
#
# Copyright (c) 2017 Mateus P. Rodrigues <mprodrigues@dragora.org>.
# Copyright (c) 2017-2019 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=libinput
version=1.12.6
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/xorg/extra"

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=http://www.freedesktop.org/software/libinput/$tarname

description="
Input device library.

Library that handles input devices for display servers and other
applications that need to directly deal with input devices.
"

homepage=http://www.x.org
license="MIT X Consortium"

# Source documentation
docs="COPYING README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    mkdir -p build
    cd build

    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
    meson setup $configure_args \
     --libdir /usr/lib${libSuffix} \
     --buildtype=release \
     --strip \
     -Ddocumentation=false \
     -Dtests=false \
     ..

    ninja
    DESTDIR="$destdir" ninja install

    cd ..

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"
}

