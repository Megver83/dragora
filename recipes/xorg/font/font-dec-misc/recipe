# Build recipe for font-dec-misc.
#
# Copyright (c) 2017 Kelsoo, <kelsoo@dragora.org>.
# Copyright (c) 2017 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=font-dec-misc
version=1.0.3
arch=noarch
release=1

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/xorg/font"

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=http://www.x.org/releases/individual/font/$tarname

description="
Font files.

This package is part of X11.
"

homepage=http://www.x.org
license="MIT X Consortium"

# Source documentation
docs="COPYING ChangeLog README"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     || true

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"

    for file in $docs
    do
        if test -e $file
        then
            cp -p $file "${destdir}${docsdir}"
        fi
    done
}

