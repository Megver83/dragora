# Build recipe for libXfont2.
#
# Copyright (c) 2017 Mateus P. Rodrigues <mprodrigues@dragora.org>.
# Copyright (c) 2017-2018 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

program=libXfont2
version=2.0.3
release=2

# Set 'outdir' for a nice and well-organized output directory
outdir="${outdir}/${arch}/xorg/lib"

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=http://www.x.org/releases/individual/lib/$tarname

description="
The core of the legacy X11 font system.

libXfont provides the core of the legacy X11 font system, handling the
index files (fonts.dir, fonts.alias, fonts.scale), the various font
file formats, nd rasterizing them.  It is used by the X servers, and
will eventually be used by the X Font Server (xfs), but should not be
used by normal X11 clients.  X11 clients access fonts via either the
new APIs in libXft, or the legacy APIs in libX11.
"

homepage=http://www.x.org
license="MIT X Consortium"

# Source documentation
docs="AUTHORS COPYING ChangeLog README"
docsdir="${docdir}/${program}-${version}"

build()
{
    set -e

    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --docdir=$docsdir \
     --enable-shared=yes \
     --enable-static=no \
     --enable-ipv6=yes \
     --enable-snfformat \
     --with-bzip2 \
     --build="$(cc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 '{}' +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}${docsdir}"
    cp -p $docs "${destdir}${docsdir}"/
}

